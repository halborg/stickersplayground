package dk.lego.stickersplayground;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.thuytrinh.android.collageviews.MultiTouchListener;

public class Main2Activity extends AppCompatActivity {

    private final static String TAG = "Main2Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Button button = (Button) findViewById(R.id.setMatrixButton1);



        final Sticker sticker1 = getIntent().getParcelableExtra(Constants.TRANSFORM_INFO_INTENT_NAME1);
        Sticker sticker2 = getIntent().getParcelableExtra(Constants.TRANSFORM_INFO_INTENT_NAME2);

//        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.main2frame);

//        ImageView imageView1 = new ImageView(this);
//        ImageView sticker = new ImageView(this);

        final ImageView imageView1 = (ImageView) findViewById(R.id.image_view3);
        ImageView imageView2 = (ImageView) findViewById(R.id.image_view4);

        imageView1.setImageDrawable(ContextCompat.getDrawable(this, sticker1.getResId()));
        imageView2.setImageDrawable(ContextCompat.getDrawable(this, sticker2.getResId()));

//        frameLayout.addView(imageView1);
//        frameLayout.addView(sticker);

        imageView1.setOnTouchListener(new MultiTouchListener(imageView1, sticker1.getTransformInfo()));
        imageView2.setOnTouchListener(new MultiTouchListener(imageView2, sticker2.getTransformInfo()));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Matrix m1 = new Matrix();
                Matrix m2 = new Matrix();

//                m1.postRotate(sticker1.getTransformInfo().deltaAngle);
                m1.postRotate(45f);
//                m1.postScale(sticker1.getTransformInfo().deltaX, sticker1.getTransformInfo().deltaY);
                m1.postScale(3f, 3f);

                imageView1.setImageMatrix(m1);
                Log.d(TAG, "m1 matrix: " + m1.toString());

            }
        });


//        String path = getIntent().getStringExtra("path");
        Bitmap bitmap = null;
//        try {
//            bitmap = BitmapFactory.decodeStream(this.openFileInput(path));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        ImageView imageView = (ImageView) findViewById(R.id.display);
//        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }
}
