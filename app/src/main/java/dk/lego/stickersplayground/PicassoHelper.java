package dk.lego.stickersplayground;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Helper-class for setting Picasso pictures
 */
public class PicassoHelper {

    public static void setScaledImageInImageView(@NonNull Context context,
                                                 @NonNull ImageView imageView, String path) {

        File file = new File(path);

        Bitmap bitmap = BitmapFactory.decodeFile(path);
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        Picasso.with(context)
                .load(file)
                .fit()
                .into(imageView);

    }

}