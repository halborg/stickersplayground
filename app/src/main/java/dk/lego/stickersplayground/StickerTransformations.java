package dk.lego.stickersplayground;

import android.util.Log;
import android.widget.ImageView;

public class StickerTransformations {

    private float scale;
    private float rotation;
    private float translationX;
    private float translationY;
    private int originalWidth;
    private int originalHeight;
    private int containingImageHeight;
    private int containingImageWidth;
    private float stickerWidthToParentRatio;
    private float stickerHeightToParentRatio;
    private float stickerTranslationXRelativeToParent;
    private float stickerTranslationYRelativeToParent;

    private float stickerToContainerRatio;

    public StickerTransformations(ImageView sticker, int containingImageHeight, int containingImageWidth) {

        this.scale = sticker.getScaleX();
        this.rotation = sticker.getRotation();
        this.translationX = sticker.getTranslationX();
        this.translationY = sticker.getTranslationY();
        this.originalHeight = sticker.getHeight();
        this.originalWidth = sticker.getWidth();

        this.containingImageHeight = containingImageHeight;
        this.containingImageWidth = containingImageWidth;

        stickerToContainerRatio = (float)originalWidth / containingImageWidth;

        stickerWidthToParentRatio = (float)originalWidth / (float)containingImageWidth;
        stickerHeightToParentRatio = (float)originalHeight / (float)containingImageHeight;

        stickerTranslationXRelativeToParent = translationX / (float)containingImageWidth;
        stickerTranslationYRelativeToParent = translationY / (float)containingImageHeight;

    }

    public void applyTransformationsToStickerImageView(ImageView sticker, ImageView backgroundImage){

        // Set size relative to parent
        int stickerWidth = getNewStickerWidthRelativeToContainingImage(backgroundImage);
        int stickerHeight = getNewStickerHeightRelativeToContainingImage(backgroundImage);

        sticker.setMinimumHeight(stickerHeight);
        sticker.setMaxHeight(stickerHeight);

        sticker.setMinimumWidth(stickerWidth);
        sticker.setMaxWidth(stickerWidth);

        // Set size relative to parent
        int stickerTranslationX = getNewStickerTranslationXRelativeToContainingImage(backgroundImage);
        float stickerTranslationY = getNewStickerTranslationYRelativeToContainingImage(backgroundImage);

        sticker.setTranslationX(stickerTranslationX);
        sticker.setTranslationY(stickerTranslationY);

        // Scale imageview - assumes uniform scaling
        sticker.setScaleX(scale);
        sticker.setScaleY(scale);

        sticker.setRotation(rotation);

        Log.d("Transform", "New sticker Height: " + sticker.getHeight());
        Log.d("Transform", "New sticker Width: " + sticker.getWidth());
        Log.d("Transform", "New sticker TransX: " + sticker.getTranslationX());
        Log.d("Transform", "New sticker TransY: " + sticker.getTranslationY());
        Log.d("Transform", "New sticker Scale: " + scale);
        Log.d("Transform", "Width Ratio: " + (float)containingImageWidth/backgroundImage.getWidth());
        Log.d("Transform", "Height Ratio: " + (float)containingImageHeight/backgroundImage.getHeight());

    }

    public int getNewStickerWidthRelativeToContainingImage(ImageView backgroundImage){
        float result = stickerWidthToParentRatio * backgroundImage.getWidth();
        return Math.round(result);
    }

    public int getNewStickerHeightRelativeToContainingImage(ImageView backgroundImage){
        float result = stickerHeightToParentRatio * backgroundImage.getHeight();
        return Math.round(result);
    }

    public int getNewStickerTranslationXRelativeToContainingImage(ImageView backgroundImage){
        float result = stickerTranslationXRelativeToParent * backgroundImage.getWidth();
        return Math.round(result);
    }

    public int getNewStickerTranslationYRelativeToContainingImage(ImageView backgroundImage){
        float result = stickerTranslationYRelativeToParent * backgroundImage.getHeight();
        return Math.round(result);
    }



    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getTranslationX() {
        return translationX;
    }

    public void setTranslationX(float translationX) {
        this.translationX = translationX;
    }

    public float getTranslationY() {
        return translationY;
    }

    public void setTranslationY(float translationY) {
        this.translationY = translationY;
    }

    public int getOriginalWidth() {
        return originalWidth;
    }

    public void setOriginalWidth(int originalWidth) {
        this.originalWidth = originalWidth;
    }

    public int getOriginalHeight() {
        return originalHeight;
    }

    public void setOriginalHeight(int originalHeight) {
        this.originalHeight = originalHeight;
    }

    public int getContainingImageHeight() {
        return containingImageHeight;
    }

    public void setContainingImageHeight(int containingImageHeight) {
        this.containingImageHeight = containingImageHeight;
    }

    public int getContainingImageWidth() {
        return containingImageWidth;
    }

    public void setContainingImageWidth(int containingImageWidth) {
        this.containingImageWidth = containingImageWidth;
    }
}
