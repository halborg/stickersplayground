package dk.lego.stickersplayground;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.thuytrinh.android.collageviews.MultiTouchListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    ImageView sticker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        final ImageView resultImage = (ImageView) findViewById(R.id.result_image);
        ImageView backgroundImageView = (ImageView) findViewById(R.id.background_imageview);

        Picasso.with(getApplicationContext())
                .load(R.drawable.ducks)
                .fit()
                .into(backgroundImageView);

        frameLayout.setDrawingCacheEnabled(true);

        sticker = (ImageView) findViewById(R.id.image_view2);
        Button button = (Button) findViewById(R.id.button);
        Button matrixButton = (Button) findViewById(R.id.matrixbutton);

        final MultiTouchListener m2 = new MultiTouchListener();

        sticker.setOnTouchListener(m2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.finish();
                startActivity(MainActivity.this.getIntent());
            }
        });

        matrixButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * 1. Create canvas with desired dimens
                 * 2. Create framelayout with same dimens
                 * 3. Set background of framelayout to the desired image OR add imageview with background to framelayout
                 * 4. Add sticker to framelayout (relative to size)
                 * 5. Draw layout to canvas
                 * 6. Save canvas
                 * 7. Display
                 */

                // 1.
                int width = 4000;
                int height = 3000;
                Bitmap bg = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bg);
                Log.d(TAG, "Transform NEW RUN --------------------------------");
                Log.d(TAG, "Transform Original sticker Height: " + sticker.getHeight());
                Log.d(TAG, "Transform Original sticker Width: " + sticker.getWidth());
                Log.d(TAG, "Transform Original sticker TransX: " + sticker.getTranslationX());
                Log.d(TAG, "Transform Original sticker TransY: " + sticker.getTranslationY());
                Log.d(TAG, "Transform Original sticker Scale: " + sticker.getScaleX());

                // 2.
                FrameLayout containingLayout = new FrameLayout(getApplicationContext());
                containingLayout.layout(0, 0, width, height);

                // 3.
                //containingLayout.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ducks));
                ImageView backgroundImageView = new ImageView(getApplicationContext());
                backgroundImageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ducks));
                backgroundImageView.layout(0,0,width,height);
                backgroundImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                containingLayout.addView(backgroundImageView);

                // 4.
                ImageView stickerResult = new ImageView(getApplicationContext());
                stickerResult.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cat3));

                StickerTransformations transformations = new StickerTransformations(sticker, frameLayout.getHeight(), frameLayout.getWidth());

                stickerResult.layout(0,0,
                        //transformations.getNewStickerTranslationXRelativeToContainingImage(containingLayout),
                        //transformations.getNewStickerTranslationYRelativeToContainingImage(containingLayout),
                        transformations.getNewStickerWidthRelativeToContainingImage(backgroundImageView),
                        transformations.getNewStickerHeightRelativeToContainingImage(backgroundImageView));

                containingLayout.addView(stickerResult);
                transformations.applyTransformationsToStickerImageView(stickerResult, backgroundImageView);

                // 5.
                containingLayout.draw(canvas);
                canvas.save();

                // 6.
                String path = saveImageToExternalStorage(bg);

                // 7.
                showBitmapInAnotherLayout(path, resultImage);

                // Save original frame as bitmap for comparison:
                //saveImageToExternalStorage(viewToBitmap(relativeLayout));

            }
        });
    }

    private void showBitmapInAnotherLayout(String path, ImageView imageView) {
        PicassoHelper.setScaledImageInImageView(getApplicationContext(), imageView, path);
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private String saveImageToExternalStorage(Bitmap finalBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Screenshots");
        if(!myDir.exists()){
            myDir.mkdir();
        }
        //myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        final File file = new File(myDir, fname);
        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(this, new String[] { file.toString() }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                        Log.i("ExternalStorage", "-> file=" + file.getAbsolutePath().toString());
                    }
                });
        return file.getAbsolutePath();
    }
}
