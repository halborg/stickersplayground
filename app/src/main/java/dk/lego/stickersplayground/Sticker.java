package dk.lego.stickersplayground;

import android.os.Parcel;
import android.os.Parcelable;

import com.thuytrinh.android.collageviews.TransformInfo;

/**
 * Created by Halborg on 28/06/2016.
 */
public class Sticker implements Parcelable {

    private int resId;
    private TransformInfo transformInfo;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public TransformInfo getTransformInfo() {
        return transformInfo;
    }

    public void setTransformInfo(TransformInfo transformInfo) {
        this.transformInfo = transformInfo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.resId);
        dest.writeParcelable(this.transformInfo, flags);
    }

    public Sticker() {
    }

    protected Sticker(Parcel in) {
        this.resId = in.readInt();
        this.transformInfo = in.readParcelable(TransformInfo.class.getClassLoader());
    }

    public static final Creator<Sticker> CREATOR = new Creator<Sticker>() {
        @Override
        public Sticker createFromParcel(Parcel source) {
            return new Sticker(source);
        }

        @Override
        public Sticker[] newArray(int size) {
            return new Sticker[size];
        }
    };
}
