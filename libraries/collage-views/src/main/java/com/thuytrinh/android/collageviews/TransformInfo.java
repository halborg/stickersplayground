package com.thuytrinh.android.collageviews;

import android.os.Parcel;
import android.os.Parcelable;

public class TransformInfo implements Parcelable {

    public float deltaX;
    public float deltaY;
    public float deltaScale;
    public float deltaAngle;
    public float pivotX;
    public float pivotY;
    public float minimumScale;
    public float maximumScale;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.deltaX);
        dest.writeFloat(this.deltaY);
        dest.writeFloat(this.deltaScale);
        dest.writeFloat(this.deltaAngle);
        dest.writeFloat(this.pivotX);
        dest.writeFloat(this.pivotY);
        dest.writeFloat(this.minimumScale);
        dest.writeFloat(this.maximumScale);
    }

    public TransformInfo() {
    }

    protected TransformInfo(Parcel in) {
        this.deltaX = in.readFloat();
        this.deltaY = in.readFloat();
        this.deltaScale = in.readFloat();
        this.deltaAngle = in.readFloat();
        this.pivotX = in.readFloat();
        this.pivotY = in.readFloat();
        this.minimumScale = in.readFloat();
        this.maximumScale = in.readFloat();
    }

    @Override
    public String toString() {
        String result = "Transform info: ";
        result += "\n deltaX = " + deltaX;
        result += "\n deltaY = " + deltaY;
        result += "\n deltaScale = " + deltaScale;
        result += "\n deltaAngle = " + deltaAngle;
        result += "\n pivotX = " + pivotX;
        result += "\n pivotY = " + pivotY;
        result += "\n minimumScale = " + minimumScale;
        result += "\n maximumScale = " + maximumScale;
        return result;
    }

    public static final Parcelable.Creator<TransformInfo> CREATOR = new Parcelable.Creator<TransformInfo>() {
        @Override
        public TransformInfo createFromParcel(Parcel source) {
            return new TransformInfo(source);
        }

        @Override
        public TransformInfo[] newArray(int size) {
            return new TransformInfo[size];
        }
    };
}